<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/top_performing_mutual_funds_in_india', function () {
    return view('mutual-funds');
});




Route::post('/get_sip_return', 'UserController@getSipReturn')->name('getSipReturn');