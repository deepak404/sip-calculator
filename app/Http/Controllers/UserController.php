<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class UserController extends Controller
{
    public function getSipReturn(Request $request){
    	$request->validate([
    		'investment_amount' => 'required',
            'investment_period' => 'required',
            'interest_rate' => 'required',
    		]);

    	$investment_amount = $request['investment_amount'];
        $interest_rate = (($request['interest_rate'])/100)/12;
        $investment_period = $request['investment_period'];
        $increment_rate = $request['increment_rate'];
        $sip_return = 0;
        $current_year = carbon::now()->year;
        $sip_investment_data = array();
        if(array_key_exists('increment_rate', $request)) {
            $increment_rate = $request['increment_rate'];
        }

        $sip_investment_data[] = array(
            'investment_year' => $current_year,
            'non_sip_amount' => 0,
            'sip_amount' => 0,
        ); 

        $initial_amount = $investment_amount;
        $new_investment_amount = $investment_amount;
        $current_investment_amount = $new_investment_amount;
        $investment_month_period = 0;
        $non_sip_amount = 0;
        
        if ($investment_period < 10) {
        	
        }else{

        }

        $investment_period += 25;

        //echo $investment_period;
        //die();
        $max_investment_month_period = ($investment_period * 12);

        // print_r($max_investment_month_period);
        // die();
        //calculating sip return sum


        while($investment_month_period < $max_investment_month_period) {
            $investment_month_period ++;
            $non_sip_amount += $initial_amount;
            $new_investment_amount += ($new_investment_amount * $interest_rate);
            if($investment_month_period % 12 == 0){
                $current_year++;
                $sip_investment_data[] = array(
                    'investment_year' => $current_year,
                    'non_sip_amount' => round($non_sip_amount),//$fmt->format(round($non_sip_amount)),
                    'sip_amount' => round($new_investment_amount),//$fmt->format(round($new_investment_amount)),
                ); 
                if($increment_rate > 0){
                    $initial_amount += (($initial_amount * $increment_rate)/100);
                }
            }
           
            if($investment_month_period < $max_investment_month_period) {
                $new_investment_amount += $initial_amount;
            }
            
        }

        // print_r($sip_investment_data);
        // die();


        $final_array = [];
        $count = 0;
        $keys = [];
        //$final_array[] = $sip_investment_data[0];

        // print_r($sip_investment_data);
        // die();

        foreach ($sip_investment_data as $key => $sip) {
        	// echo $key;


        	//if ($request['investment_period'] < 15) {
        		//echo $request['investment_period'];

	        	if ($sip['investment_year'] == ($request['investment_period'] + Carbon::now()->year)) {
      				$final_array[] = $key;
      				//print_r($sip_investment_data[$key]);
      				//die();
      			}
        	//}
      		// elseif ($request['investment_period'] > 15) {
      		// 	if ($sip['investment_year'] == ($request['investment_period'] + Carbon::now()->year)) {
      		// 		$final_array[] = $key;
      		// 	}
        		
        // 	}elseif ($request['investment_period'] > 90) {
        // 		if ($sip['investment_year'] == ($request['investment_period'] + Carbon::now()->year)) {
      		// 		$final_array[] = $key;
      		// 	}
        // 	}
        } //foreach ends


        $pass_array = [];
        if ($request['investment_period'] < 15) {
        	$key = $final_array[0];
  			$pass_array[] = $sip_investment_data[$key];
            $pass_array[] = $sip_investment_data[$key + 5];
            $pass_array[] = $sip_investment_data[$key + 10];
            $pass_array[] = $sip_investment_data[$key + 15];
            $pass_array[] = $sip_investment_data[$key + 20];
    	}
  		elseif ($request['investment_period'] >= 15 && $request['investment_period'] < 90) {
  			$key = $final_array[0];
            $pass_array[] = $sip_investment_data[$key - 10];
            $pass_array[] = $sip_investment_data[$key - 5];
  			$pass_array[] = $sip_investment_data[$key];
			$pass_array[] = $sip_investment_data[$key + 5];
			$pass_array[] = $sip_investment_data[$key + 10];
    		
    	}else{
    		$key = $final_array[0];
			$pass_array[] = $sip_investment_data[$key - 5];
            $pass_array[] = $sip_investment_data[$key - 10];
            $pass_array[] = $sip_investment_data[$key - 15];
            $pass_array[] = $sip_investment_data[$key - 20];
            $pass_array[] = $sip_investment_data[$key];
    	}

        // http_response_code(500);
        // dd($pass_array);
    	// $temp = $pass_array[2];
    	// $pass_array[2] = $pass_array[0];
    	// $pass_array[0] = $temp;

     //    if ($request['investment_period'] <= 5) {
     //       $temp = $pass_array[2];
     //       $pass_array[2] = $pass_array[0];
     //       $pass_array[0] = $temp;
     //    }


        
        return response()->json(['msg' => 'success', 'sip_return' => round($new_investment_amount), 'sip_details' => $pass_array]);
    }
}
