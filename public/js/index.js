$(document).ready(function(){

	var sip_amount,non_sip_amount;

	$('#inv-period').keydown(function(e){
		if ($(this).val().length <= 1) {
			if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 95 && e.keyCode < 106)  || e.keyCode == 8 || e.keyCode == 9 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
				return true;
			}else{
				return false;
			}
		}else{
			if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
	            return true;
	        }else{
	            return false;
	        }   

	        e.preventDefault();  
		}
	})


	$(document).on('keydown','.num-field',function(e){
		//console.log(e.keyCode);
		if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 95 && e.keyCode < 106)  || e.keyCode == 8 || e.keyCode == 9 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
			return true;
		}else{
			return false;
		}
	});

	
	var date = new Date();
	var year = date.getFullYear();
	//console.log(year);
	$('#go').on('click',function(e){
		var inv_amount = $('#inv-amount').val();
		var ret_per = $('#exp-return').val();
		var inv_period = $('#inv-period').val();
		var ann_incr = $('#ann-increment').val();

		var dataString = 'investment_amount='+inv_amount+'&investment_period='+inv_period+ '&interest_rate='+ret_per+ '&increment_rate=' +ann_incr;
	
		//console.log(dataString);

		$.ajax({
	        type: "POST",
	        url: "/get_sip_return",
	        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	        cache: false,
	        processData: false,
	        //async: false,
	        data: dataString,
	        success: function(data) {
	          	if (data.msg = "success") {
	          		generateReturns(data)
	          	}
	        },
	        error : function(xhr ,status ,error)
	        {
	          //console.log(status);
	        },
	      });
		e.preventDefault();
	});

	function generateReturns(data){
		var returns = data.sip_details;
		var count = 0;
		var class_name = "";
		var chart_amount_invested;
		var chart_wealth_gained;
		$('#sip-body').empty();
		var return_amount;
		// returns.sort(function(a, b) {
		// 	return a.investment_year - b.investment_year;
		// });
		console.log(returns);
		$.each(returns,function(k,v){
			count++;
			//console.log(count);



			sip_amount = String(v.sip_amount);
			//console.log(sip_amount+" - "+sip_amount.length);
			sip_amount = formatNumber(sip_amount);
			non_sip_amount = String(v.non_sip_amount);
			//console.log(non_sip_amount);
			non_sip_amount = formatNumber(non_sip_amount);
			//console.log(non_sip_amount);
			if ($('#inv-period').val() < 15) {
				if (count == 1) {
					class_name = "active-row";
					appendValues(v)
				}else{
					class_name = '';
				}
			}
			else if (($('#inv-period').val() >= 15 ) &&  ($('#inv-period').val() < 90 )){
				if (count == 3) {
					class_name = "active-row";
					appendValues(v)
				}else{
					class_name = '';
				}
			}
			else if ($('#inv-period').val() >= 90) {
				if (count == 5) {
					class_name = "active-row";
					appendValues(v)
				}else{
					class_name = '';
				}
			}
			// if (count == 1) {
			// 	if ($('#inv-period').val() <= 5 ) {
			// 		//console.log('here')
			// 		class_name = "active-row";
			// 		appendValues(v)
			// 		//alert(class_name);
			// 	}
			// }


			// else if (count == 3) {

			// 	if ($('#inv-period').val() > 5 ) {
			// 		//console.log('gt5')
			// 		class_name = "active-row";
			// 		appendValues(v)
			// 	}
				
			// 	// appendValues(v)


			// }else{
			// 	class_name = " ";
			// }
			
			//console.log(class_name);
			$('#chart-amount-invested').data('value',v.non_sip_amount);
			$('#chart-wealth-gained').data('value',v.sip_amount);
			$('#return-amount').text()

			$('#chart').donut();

			$('#sip-body').append(
				'<tr class = "'+class_name+'">'+
	                '<td>'+(v.investment_year - year)+' Years</td>'+
	                '<td>'+non_sip_amount+'</td>'+
	                '<td>'+sip_amount+'</td>'+
	            '</tr>')
		});
	}

	function formatNumber(sip_amount){

		if (sip_amount.length == 6) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+"."+sip_amount[1]+" Lkhs.";
			//console.log("7 - "+sip_amount);
		}

		else if (sip_amount.length == 7) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+""+sip_amount[1]+"."+sip_amount[2]+" Lkhs.";
			//console.log("7 - "+sip_amount);
		}

		else if (sip_amount.length == 8) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+"."+sip_amount[1]+" Crs.";
		}

		else if (sip_amount.length == 9) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+""+sip_amount[1]+"."+sip_amount[2]+" Crs.";
		}

		else if (sip_amount.length == 10) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+""+sip_amount[1]+""+sip_amount[2]+"."+sip_amount[3]+" Crs.";
		}

		else if (sip_amount.length == 11) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+","+sip_amount[1]+""+sip_amount[2]+""+sip_amount[3]+"."+sip_amount[4]+" Crs.";
		}

		else if (sip_amount.length == 12) {
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+""+sip_amount[1]+","+sip_amount[2]+""+sip_amount[3]+""+sip_amount[4]+"."+sip_amount[5]+" Crs.";
		}

		else if (sip_amount.length == 13){
			//console.log(sip_amount);
			sip_amount = sip_amount.split("");
			sip_amount = sip_amount[0]+","+sip_amount[1]+""+sip_amount[2]+","+sip_amount[3]+""+sip_amount[4]+""+sip_amount[5]+" Crs.";
		}else{

		}

		//console.log(sip_amount);
		return sip_amount;
	}

	//inrformat();

	function inrformat(data) {
         var x=data;
         x=x.toString();
         var afterPoint = '';
         if(x.indexOf('.') > 0)
            afterPoint = x.substring(x.indexOf('.'),x.length);
         x = Math.floor(x);
         x=x.toString();
         var lastThree = x.substring(x.length-3);
         var otherNumbers = x.substring(0,x.length-3);
         if(otherNumbers != '')
             lastThree = ',' + lastThree;
         var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
         return res;
   }

   	$(document).ajaxStart(function(){
       $("#loader").css("display", "block");
       $('section,nav').css({'opacity':'0.3'});
       $('#ajax-info').show();

	});

	$(document).ajaxComplete(function(){
	   $("#loader").css("display", "none");
	   $('section,nav').css({'opacity':'1'});
	   $('#ajax-info').hide();
	});


	function appendValues(v){
		chart_amount_invested = v.non_sip_amount;
				chart_wealth_gained = v.sip_amount;
				return_amount = v.sip_amount;

				var wealth_gained = parseInt(v.sip_amount) - parseInt(v.non_sip_amount);
				var fr_w_g = formatNumber(String(wealth_gained));

				v.sip_amount = inrformat(v.sip_amount);
				v.non_sip_amount = inrformat(v.non_sip_amount);

				// $('#expected-amount').text("Rs."+v.sip_amount+" (Rs. "+sip_amount+")");
				// $('#amount-invested').text("Rs."+v.non_sip_amount+" (Rs. "+non_sip_amount+")");

				// $('#return-amount').text('Rs.' +sip_amount);
				
				// // alert(fr_w_g);
				// wealth_gained = inrformat(wealth_gained);

				// $('#wealth-gained').text("Rs."+wealth_gained+" (Rs. "+fr_w_g+")");


				$('#expected-amount').text("Rs."+v.sip_amount);
				$('#amount-invested').text("Rs."+v.non_sip_amount);

				$('#return-amount').text('Rs.' +sip_amount);
				
				// alert(fr_w_g);
				wealth_gained = inrformat(wealth_gained);

				$('#wealth-gained').text("Rs."+wealth_gained);

				$('#graph-wealth-gained').text("Rs. "+fr_w_g+"");
				$('#graph-amount-invested').text("Rs. "+non_sip_amount+"");
	}
});