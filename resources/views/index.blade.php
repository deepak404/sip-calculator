<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> SIP Calculator - SIPCalculator.io</title>
        <meta name="description" content="Online SIP Calculator - calculate your returns on mutual fund investments. Simple Interface, Top performing mutual funds, Start your Systematic Investment Plan today.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111077749-1"></script>
        <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-111077749-1');
        </script>
    </head>
    <body>
    <div class="loader" id="loader"></div>
    <p id="ajax-info">Calculating...</p>
        


<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
  <div class="container-fluid" id="navbar-container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#"><img src="images/logo.svg" class="nav-logo"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class="active-menu"><a href="/top_performing_mutual_funds_in_india">Top Performing Mutual Funds</a></li>
      </ul>
    </div>
  </div>
</nav>


    <section id="sip-calculator">
        <div class="container-fluid">
            <div class="row">
                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class = "col-lg-2 col-md-2 col-sm-2">  </div>
                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-10 sip-calc-container">
                        <h1>Sip Calculator</h1>
                        <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow-all padding-zero br sip-details-holder ">
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 user-row">
                                <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <p class="input-helper">Monthly Investment - Rs.</p>
                                    <div class="form-group">
                                        <input type="text" name="investment-amount" class = "num-field" id="inv-amount" value="10000" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <p class="input-helper">Expected Return - %.</p>
                                    <div class="form-group">
                                        <input type="text" name="exp-return" class = "num-field" id="exp-return" value="15" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                    <p class="input-helper">Investment Period - Yrs.</p>
                                    <div class="form-group">
                                        <input type="text" name="inv-period" class = "num-field" id="inv-period" value="25" required>
                                        <span class="text-danger"></span>
                                    </div>
                                </div>
                                <div class = "col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <p class="input-helper">Annual Increment - %</p>
                                    <div class="form-group" id="exp-ret-div">
                                        <input type="text" name="ann-increment" class = "num-field" id="ann-increment" value="0" required>
                                        <span class="text-danger"></span>
                                    </div>
                                    <span class="pull-right" id="exec-holder"><a href="#" id="go">Go <!-- <i class="material-icons">keyboard_arrow_right</i> --></a></span>
                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="return-details">
                                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <p class="info-header">Expected Amount</p>
                                    <p class="info-details" id="expected-amount">Rs.3,28,40,737</p>
                                </div>
                                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <p class="info-header">Amount Invested</p>
                                    <p class="info-details" id="amount-invested">Rs.30,00,000</p>
                                </div>
                                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <p class="info-header">Wealth Gained</p>
                                    <p class="info-details" id="wealth-gained">Rs.2,98,40,737</p>
                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="projected-ret-holder">
                                <div class = "col-lg-4 col-md-4 col-sm-12 col-xs-12 sip-ret-graph">
                                    <h4 class="pro-header">Amount Invested vs Return</h4>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-zero">
                                        <canvas id="chart" height="250" width="250">
                                          <div id="chart-amount-invested" data-value="1500000"></div>
                                          <div id="chart-wealth-gained" data-value="15000000"></div>
                                        </canvas>
                                    <p class="blue-text" id="return-amount">3.2 Crs.</p>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12" id="graph-det-info">
                                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-zero">
                                            <span id="wealth-gain-span"></span>
                                            <p class="graph-info-header">Wealth Gain</p>
                                            <p class="graph-info-detail" id="graph-wealth-gained">Rs. 2.9 Crs.</p>
                                        </div>
                                        <div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-zero">
                                            <span id="amount-inv-span"></span>
                                            <p class="graph-info-header">Amount Invested</p>
                                            <p class="graph-info-detail" id="graph-amount-invested">Rs. 30.0 Lkhs.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class = "col-lg-8 col-md-8 col-sm-12 col-xs-12 sip-ret-table">
                                    <h4 class="pro-header">Projected SIP Return</h4>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Duration</th>
                                                <th>SIP Amount</th>
                                                <th>Future Value</th>
                                            </tr>
                                        </thead>
                                        <tbody id="sip-body">
                                            <tr>
                                                <td>15 Years</td>
                                                <td>18.0 Lkhs.</td>
                                                <td>67.6 Lkhs.</td>
                                            </tr>
                                            <tr>
                                                <td>20 Years</td>
                                                <td>24.0 Lkhs.</td>
                                                <td>1.5 Crs.</td>
                                            </tr>
                                            <tr class="active-row">
                                                <td>25 Years</td>
                                                <td>30.0 Lkhs.</td>
                                                <td>3.2 Crs.</td>
                                            </tr>
                                            <tr>
                                                <td>30 Years</td>
                                                <td>36.0 Lkhs.</td>
                                                <td>7.0 Crs.</td>
                                            </tr>
                                            <tr>
                                                <td>35 Years</td>
                                                <td>42.0 Lkhs.</td>
                                                <td>14.8 Crs.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class = "col-lg-2 col-md-2 col-sm-2 ad-banner"></div>
                </div>
            </div>
        </div>
    </section>


     <script src="js/jquery.min.js"></script>
     <script src="js/jquery.donut.js?v=1.1"></script>
     <script src="js/index.js?v=1.1"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script>
         $(document).ready(function(){
            $('#chart').donut();
         });
     </script>
    </body>
</html>
